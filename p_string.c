/*
 * project: String
 * created: Tue Aug 15 10:30:24 2017
 * creator: christian
*/

#include "p_string.h"

void strFree(String *del) {
  free(del->value);
}

int init(String *newStr) {
  newStr->size = 0;
  newStr->value = calloc(PAGESIZE, sizeof(char));
  if (newStr->value == NULL) {
    return -1;
  }
  newStr->pages = 1;
  newStr->len = 0;
  return 0;
}


int add(String *str, char add) {

  if ((str->size + 1) >= PAGESIZE) {
    char *tmp;
    str->pages++;
    str->size = 0;
    tmp = realloc(str->value, PAGESIZE * str->pages);
    if (tmp == NULL) {
      return -1;
    }
    str->value = tmp;
  }
  str->value[str->len] = add;
  str->len++;
  str->value[str->len] = '\0';
  str->size++;
  return 0;
}


int concat(String *des, char *source) {
  while (*source != '\0') {
    add(des, *source);
    source++;
  }
}

void removeTok(String *str, unsigned int index) {
  for (; index < str->len; index++) {
    str->value[index] = str->value[index + 1];
  }
  str->len--;
  str->size--;
}

char *substring(String *str, unsigned int beginIndex, unsigned int endIndex) {

  char *re = calloc(str->len - (str->len - (beginIndex + (str->len - endIndex))), sizeof(char));

  if (re == NULL) {
    return NULL;
  }
  int i;
  for (i = 0; beginIndex <= endIndex; i++) {
    re[i] = str->value[beginIndex];

    beginIndex++;
  }
  return re;
}
